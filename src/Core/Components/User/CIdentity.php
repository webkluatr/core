<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 14.12.2017
 * Time: 10:45
 */

namespace Kluatr\Core\Components\User;
use Yii;
use yii\web\IdentityInterface;
use yii\base\Component;


/**
 * Class CIdentity
 * Основной инструмент идендификации пользователя
 * @package App\Components\Components\User
 */
class CIdentity extends Component implements IdentityInterface,IBaseUser
{
    protected $id;
    protected $email;

    public $authKey = '936c1f96-fd7c-4c33-a688-d22e700d072e';
    public $login;
    public $password;

    protected $_state=[];
    public $isValid = false;
    public $LastMessage;

    protected $user;

    public function __construct($login=null,$password=null){
        $this->login=$login;
        $this->password=$password;
    }


    public function chekLogin(){
        $pass = \hash('md5',$this->password);
        // @todo: chek login and pass next load user info
        //
        return $this->isValid;
    }

    public function getUserInfo(){
        return $this->user;
    }

    public function getTokenInfo(){
        return \Kluatr\Core\Helpers\packaging_object_to_string($this->user);
    }



    public function chekToken($token){
        $user = \Kluatr\Core\Helpers\un_packaging_string_to_object($token);
        //@todo нужен каст после распаковки объекта
        $this->user = $user;
        $this->isValid = $this->user->UserWid>0;
        $this->id = $this->user->UserWid;
        //@todo: нужна проверка токена
        return $this->isValid;
    }

    /**
     * Gets the persisted state by the specified name.
     * @param string $name the name of the state
     * @param mixed $defaultValue the default value to be returned if the named state does not exist
     * @return mixed the value of the named state
     */
    public function getState($name,$defaultValue=null)
    {
        return isset($this->_state[$name])?$this->_state[$name]:$defaultValue;
    }
    /**
     * Sets the named state with a given value.
     * @param string $name the name of the state
     * @param mixed $value the value of the named state
     */
    public function setState($name,$value)
    {
        $this->_state[$name]=$value;
    }

    public static function findIdentity($id)
    {
        echo '3';
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        echo '1';
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        echo '2';
        return $this->authKey === $authKey;
    }
}