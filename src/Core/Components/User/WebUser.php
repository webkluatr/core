<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 14.12.2017
 * Time: 10:53
 */

namespace Kluatr\Core\Components\User;

use Yii;
use yii\web\User as BaseUser;
use yii\web\IdentityInterface;


/**
 * Class WebUser
 * Основной компонент работы с пользователем
 * @package App\Components\Base\User
 */
class WebUser extends BaseUser implements IBaseUser
{
    private $_user;

    public function init(){
        parent::init();
        if (\Yii::$app->getRequest()->getCookies()->has(IBaseUser::AUTH_TOKEN_NAME) || isset($_COOKIE[IBaseUser::AUTH_TOKEN_NAME])){
            $identity = new CIdentity();
            $token = (Yii::$app->getRequest()->getCookies()->has(IBaseUser::AUTH_TOKEN_NAME))
                ? Yii::$app->request->cookies->getValue(IBaseUser::AUTH_TOKEN_NAME)
                :$_COOKIE[IBaseUser::AUTH_TOKEN_NAME];
            if($identity->chekToken($token))
                $this->login($identity);
        }
    }
    private function _get_access_keys(){
        //@todo: get access keys by active user
    }

    public function can($permissionName, $params = [], $allowCaching = true){
        // @todo: realize access shema
        $allow = false;
        #$access_keys = $this->_get_access_keys();
        #if(empty($access_keys)){
        #    return $allow;
        #}
        #foreach ($access_keys as $access){
        #    if($access->key_name == $permissionName){
        #        $allow = \filter_var($access->key_access,\FILTER_VALIDATE_BOOLEAN);
        #        break;
        #    }
        #}
        return $allow;
    }
    public function getUser(){
        return $this->_user;
    }

    public function login(IdentityInterface $identity, $duration = 0)
    {
        if ($this->beforeLogin($identity, false, $duration)) {
            $this->switchIdentity($identity, $duration);
            $id = $identity->getId();
            $ip = Yii::$app->getRequest()->getUserIP();
            if ($this->enableSession) {
                $log = "User '$id' logged in from $ip with duration $duration.";
            } else {
                $log = "User '$id' logged in from $ip. Session not enabled.";
            }
            $cookies = Yii::$app->getResponse()->getCookies();
            if($identity->isValid){
                $this->_user = $identity->getUserInfo();
                $info = $identity->getTokenInfo();
                $cookie = new \yii\web\Cookie([
                    'name' => IBaseUser::AUTH_TOKEN_NAME,
                    'value' => $info,
                    'expire'=>time()+IBaseUser::EXPIRE_LOGIN_SECONDS,
                    //'domain'=>'.com',
                    'secure'=>false,
                    'httpOnly' =>false
                ]);
                $cookies->add($cookie);
                Yii::$app->getResponse()->setCookies();
            }
            Yii::info($log, __METHOD__);
            $this->afterLogin($identity, false, $duration);
        }
        return !$this->getIsGuest();
    }

    public function logout($destroySession = true){
        $cookies = Yii::$app->getResponse()->getCookies();
        $cookie = new \yii\web\Cookie([
            'name' => IBaseUser::AUTH_TOKEN_NAME,
            'value' => "",
            'expire'=>time()-IBaseUser::EXPIRE_LOGIN_SECONDS,
            //'domain'=>'.com',
            'secure'=>false,
            'httpOnly' =>false
        ]);
        $cookies->add($cookie);
        Yii::$app->getResponse()->setCookies();
        parent::logout($destroySession);
    }
}