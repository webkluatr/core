<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 02.02.18
 * Time: 17:27
 */

namespace Kluatr\Core\Components\Cache;

use yii\redis\Cache as Cache;
/**
 * Redis short summary.
 *
 * Redis description.
 *
 * @version 1.0
 * @author p.bunaev
 */
class Redis extends Cache {

    /**
     * Возвращает значение кэша с ключом $id
     * @param mixed $key
     * @return mixed
     */
    public function getValue($key){
        return parent::getValue($key);
    }

    /**
     * Сохраняет значение $value в кэш с ключом $id на время $expire (в секундах)
     * @param string $id Имя ключа
     * @param mixed $value Значение ключа
     * @param integer $expire Время хранения в секундах
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return boolean
     */
    public function setValue($key, $value, $expire){
        return parent::setValue($key, $value, $expire);
    }


    /**
     * Удаляет кэш с ключом $id
     * @param string $id Имя ключа
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return boolean
     */
    public function delete($key){
        return parent::DeleteValue($key);
    }


    public function rpush($list,$value){
        return (bool) $this->redis->executeCommand('RPUSH', [$list, $value]);
    }

    public function executeCommand($command,$params){
        return $this->redis->executeCommand('GET', [$params]);
    }
}