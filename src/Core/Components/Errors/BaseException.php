<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 23.01.18
 * Time: 15:27
 */

namespace Kluatr\Core\Components\Errors;
use Throwable;

class BaseException extends \Exception
{
    private $_info_tpl = "
                 code: -code-
                 message: -message-
                 file: -file-
                 line: -line-
                 stack: -stack-
                 =====";

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Возвращает шаблон текстового представления ошибки
     * @return string
     */
    public function getErrorTpl():string{
        return $this->_info_tpl;
    }

    /**
     * Возвращает текстовую информацию по исключению
     * и вложенным ошибкам
     * @return string
     */
    public function getErrorStringInfo() :string {
        $error = $this;
        $info = "";
        while(true){
            $info .= \str_replace(["-code-","-message-","-file-","-line-","-stack-"],
                [$error->getCode(),$error->getMessage(),$error->getFile(),$error->getLine(),$error->getTraceAsString()],$this->_info_tpl);
            $error = $error->getPrevious();
            if (empty($error) === true) break;
            $info .= "\n INNER ERROR:";
        }
        return $info;
    }
}