<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 22.01.18
 * Time: 13:49
 */

namespace Kluatr\Core\Components\Db;


interface ICommand
{
    public function queryOne($fetchMode = null);
    public function queryAll($fetchMode = null);

    public function setFetchMode($mode,$class=null) : ICommand;

    public function bindParam($name, &$value, $dataType = null, $length = null, $driverOptions = null);
    public function bindValue($name, $value, $dataType = null);
    public function bindValues($values);

    public function query();

}