<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 11:09
 */

namespace Kluatr\Core\Components\Db;

use yii\db\Command as BaseCommand;
class CCommand extends BaseCommand implements ICommand
{

    public function setFetchMode($mode,$class=null) : ICommand {
        $this->fetchMode = [$mode,$class];
        return $this;
    }
    /**
     * Performs the actual DB query of a SQL statement.
     * @param string $method method of PDOStatement to be called
     * @param integer $fetchMode the result fetch mode. Please refer to [PHP manual](http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php)
     * for valid fetch modes. If this parameter is null, the value set in [[fetchMode]] will be used.
     * @return mixed the method execution result
     * @throws Exception if the query causes any problem
     * @since 2.0.1 this method is protected (was private before).
     */
    protected function queryInternal($method, $fetchMode = null)
    {
        $rawSql = $this->getRawSql();
        \Yii::info($rawSql, 'yii\db\Command::query');
        if ($method !== '') {
            $info = $this->db->getQueryCacheInfo($this->queryCacheDuration, $this->queryCacheDependency);
            if (is_array($info)) {
                /* @var $cache \yii\caching\Cache */
                $cache = $info[0];
                $cacheKey = [
                    __CLASS__,
                    $method,
                    $fetchMode,
                    $this->db->dsn,
                    $this->db->username,
                    $rawSql,
                ];
                $result = $cache->get($cacheKey);
                if (is_array($result) && isset($result[0])) {
                    \Yii::trace('Query result served from cache', 'yii\db\Command::query');
                    return $result[0];
                }
            }
        }
        $this->prepare(true);
        $token = $rawSql;
        try {
            \Yii::beginProfile($token, 'yii\db\Command::query');
            $this->pdoStatement->execute();
            if ($method === '') {
                $result = new \DataReader($this);
            } else {
                if ($fetchMode === null) {
                    $fetchMode = $this->fetchMode;
                }

                \call_user_func_array([$this->pdoStatement, 'setFetchMode'], (array) $fetchMode);
                $result = \call_user_func_array([$this->pdoStatement, $method], []);
                $this->pdoStatement->closeCursor();
            }
            \Yii::endProfile($token, 'yii\db\Command::query');
        }
        catch (\Exception $e) {
            \Yii::endProfile($token, 'yii\db\Command::query');
            throw $this->db->getSchema()->convertException($e, $rawSql);
        }
        if (isset($cache, $cacheKey, $info)) {
            $cache->set($cacheKey, [$result], $info[1], $info[2]);
            \Yii::trace('Saved query result in cache', 'yii\db\Command::query');
        }
        return $result;
    }
}