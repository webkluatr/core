<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 11:00
 */

namespace Kluatr\Core\Components\Web;

use yii\web\Response;
class CResponse extends Response
{
    private $_cookies;

    public function send()
    {

        if ($this->isSent) {
            return;
        }

        //$this->sendCookies();
        $this->trigger(self::EVENT_BEFORE_SEND);
        $this->prepare();
        $this->trigger(self::EVENT_AFTER_PREPARE);
        $this->sendHeaders();
        $this->sendContent();
        $this->trigger(self::EVENT_AFTER_SEND);
        $this->isSent = true;
    }


    /**
     * Sends the cookies to the client.
     */
    protected function sendCookies()
    {
        parent::sendCookies();
    }

    public function addCookieItem($cookie){
        $this->getCookies()->add($cookie);
        $this->sendCookies();
    }
    public function removeCookieItem($cookieName){
        $this->getCookies()->remove($cookieName);
        $this->getCookies()->remove($cookieName,false);
        $this->sendCookies();
    }

    public function setCookies(){
        $this->sendCookies();
    }
}
