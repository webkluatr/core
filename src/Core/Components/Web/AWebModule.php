<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 11:03
 */

namespace Kluatr\Core\Components\Web;

abstract class AWebModule extends \yii\base\Module{
    public $urlRules = [];


    public function init(){
        parent::init();
    }

    protected function _add_url_rule($key,$path,$verb=['GET', 'HEAD','POST']){
        $this->urlRules[]=['route'=>$path,'pattern'=>$key,'verb'=>$verb,'class'=>'Kluatr\Core\Components\Web\Managers\CUrlRule'];
    }

}
