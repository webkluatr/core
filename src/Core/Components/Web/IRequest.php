<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 02.02.18
 * Time: 12:26
 */

namespace Kluatr\Core\Components\Web;


interface IRequest
{
    public function allParams();

    public function getParams();
    public function postParams();

}