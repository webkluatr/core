<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 10:59
 */

namespace Kluatr\Core\Components\Web;

class CRequest extends \yii\web\Request implements IRequest
{
    private $_cookies;

    public function getCookies()
    {
        if ($this->_cookies === null) {
            $this->_cookies = new CCookieCollection($this->loadCookies(), [
                'readOnly' => true,
            ]);
        }
        return $this->_cookies;
    }

    public function allParams()
    {
        return $this->bodyParams;
    }

    public function getParams()
    {
        return $this->get();
    }

    public function postParams()
    {
        return $this->post();
    }


}