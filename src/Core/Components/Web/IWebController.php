<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 02.02.18
 * Time: 12:30
 */

namespace Kluatr\Core\Components\Web;


interface IWebController extends IController
{
    public function setCaption(string $caption) : IController;
    public function getCaption() : string;
    public function setPageTitle(string $value) : IController;
    public function getPageTitle() : string;
    public function addBreadcrumb(string $caption, string $link):IController;
    public function addBreadcrumbs(array $breadcrumbs): IController;
    public function render($view, $params = []) : string;
}