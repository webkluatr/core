<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 22.01.18
 * Time: 13:55
 */

namespace Kluatr\Core\Components\Web;


interface IController
{
    public function init();

}