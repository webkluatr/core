<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 11:02
 */

namespace Kluatr\Core\Components\Web;

use yii\web\Controller as BaseController;

abstract class AWebController extends BaseController implements IWebController
{
    protected $_pageTitle;
    protected $_caption;
    public $breadcrumbs = [];

    public $layout = "@currentTheme/views/layouts/main";

    public function init(){
	parent::init();
    }

    /**
     * Установить заголовок
     * @param string $caption
     * @return IController
     **/
    public function setCaption(string $caption) : IController {
        $this->_caption = $caption;
        return $this;
    }
    /**
     * Вернуть заголовок
     * @return string
     **/
    public function getCaption() : string {
        return $this->_caption;
    }

    /**
     * Установить заголовок страницы
     * @param string $value
     * @return IController
     */
    public function setPageTitle(string $value) : IController{
        $this->_pageTitle = $value;
        $this->_caption= $value;
        return $this;
    }

    /**
     * Возвращает Заголовок страницы
     * @return string
     */
    public function getPageTitle() : string {
        return $this->_pageTitle;
    }

    /**
     * добавить одну запись в цепочку навигации
     * @param string $caption заголовок ссылки
     * @param  string $link значение ссылки
     * @return IController
     **/
    public function addBreadcrumb(string $caption, string $link):IController {
        $this->breadcrumbs[$caption] = $link;
        return $this;
    }
    /**
     * добавить массив в цепочку навигации
     * @param array $breadcrumbs массив хлебных крошек
     * @return IController
     **/
    public function addBreadcrumbs(array $breadcrumbs) : IController{
        $this->breadcrumbs = \array_merge($this->breadcrumbs, $breadcrumbs);
        return $this;
    }

    /**
     * Выполняет отрисовку шаблона
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = []):string {
        $view = "@currentTheme/views/".\strtolower($this->module->id)."/".$this->id."/".$view;
        return parent::render($view,$params);
    }
}
