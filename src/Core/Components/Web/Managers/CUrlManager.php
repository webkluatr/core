<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 27.12.2017
 * Time: 15:33
 */

namespace Kluatr\Core\Components\Web\Managers;

use yii\web\UrlManager as BaseUrlManager;
/**
 * CUrlManager short summary.
 *
 * CUrlManager description.
 *
 * @version 1.0
 * @author p.bunaev
 */
class CUrlManager extends BaseUrlManager
{
    /**
     * Parses the user request.
     * @param Request $request the request component
     * @return array|boolean the route and the associated parameters. The latter is always empty
     * if [[enablePrettyUrl]] is false. False is returned if the current request cannot be successfully parsed.
     */
    public function parseRequest($request)
    {
        if ($this->enablePrettyUrl) {
            $pathInfo = $request->getPathInfo();
            $pathInfo = rtrim($pathInfo,'/');
            $request->setPathInfo($pathInfo."/");
            /* @var $rule UrlRule */
            foreach ($this->rules as $rule) {
                if (($result = $rule->parseRequest($this, $request)) !== false) {
                    return $result;
                }
            }
            if ($this->enableStrictParsing) {
                return false;
            }
            \Yii::trace('No matching URL rules. Using default URL parsing logic.', __METHOD__);
            // Ensure, that $pathInfo does not end with more than one slash.
            if (strlen($pathInfo) > 1 && substr_compare($pathInfo, '//', -2, 2) === 0) {
                return false;
            }
            $suffix = (string) $this->suffix;
            if ($suffix !== '' && $pathInfo !== '') {
                $n = strlen($this->suffix);
                if (substr_compare($pathInfo, $this->suffix, -$n, $n) === 0) {
                    $pathInfo = substr($pathInfo, 0, -$n);
                    if ($pathInfo === '') {
                        // suffix alone is not allowed
                        return false;
                    }
                } else {
                    // suffix doesn't match
                    return false;
                }
            }
            return [$pathInfo, []];
        } else {
            \Yii::trace('Pretty URL not enabled. Using default URL parsing logic.', __METHOD__);
            $route = $request->getQueryParam($this->routeParam, '');
            if (is_array($route)) {
                $route = '';
            }
            return [(string) $route, []];
        }
    }
}