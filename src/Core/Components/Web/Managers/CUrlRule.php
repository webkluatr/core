<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 27.12.2017
 * Time: 15:32
 */

namespace Kluatr\Core\Components\Web\Managers;

use yii\web\UrlRule as BaseRule;
use yii\base\InvalidConfigException;

class CUrlRule extends BaseRule
{
    private $_routeParams;
    private $_paramRules;
    private $_routeRule;
    private $_template;
    /**
     * Initializes this rule.
     */
    public function init()
    {
        if ($this->pattern === null) {
            throw new InvalidConfigException('UrlRule::pattern must be set.');
        }
        if ($this->route === null) {
            throw new InvalidConfigException('UrlRule::route must be set.');
        }
        if ($this->verb !== null) {
            if (\is_array($this->verb)) {
                foreach ($this->verb as $i => $verb) {
                    $this->verb[$i] = \strtoupper($verb);
                }
            } else {
                $this->verb = [\strtoupper($this->verb)];
            }
        }
        if ($this->name === null) {
            $this->name = $this->pattern;
        }
        $this->pattern = \trim($this->pattern, '/');
        $this->route = \trim($this->route, '/');
        if ($this->host !== null) {
            $this->host = \rtrim($this->host, '/');
            $this->pattern = \rtrim($this->host . '/' . $this->pattern, '/');
        } elseif ($this->pattern === '') {
            $this->_template = '';
            $this->pattern = '#^$#u';
            return;
        } elseif (($pos = \strpos($this->pattern, '://')) !== false) {
            if (($pos2 = \strpos($this->pattern, '/', $pos + 3)) !== false) {
                $this->host = \substr($this->pattern, 0, $pos2);
            } else {
                $this->host = $this->pattern;
            }
        } else {
            $this->pattern = '/' . $this->pattern . '/';
        }
        if (\strpos($this->route, '<') !== false && \preg_match_all('/<([\w._-]+)>/', $this->route, $matches)) {
            foreach ($matches[1] as $name) {
                $this->_routeParams[$name] = "<$name>";
            }
        }
        $tr = [
            '.' => '\\.',
            '*' => '\\*',
            '$' => '\\$',
            '[' => '\\[',
            ']' => '\\]',
            '(' => '\\(',
            ')' => '\\)',
        ];
        $tr2 = [];
        if (\preg_match_all('/<([\w._-]+):?([^>]+)?>/', $this->pattern, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $name = $match[1][0];
                $pattern = isset($match[2][0]) ? $match[2][0] : '[^\/]+';
                $placeholder = 'a' . \hash('crc32b', $name); // placeholder must begin with a letter
                $this->placeholders[$placeholder] = $name;
                if (\array_key_exists($name, $this->defaults)) {
                    $length = \strlen($match[0][0]);
                    $offset = $match[0][1];
                    if ($offset > 1 && $this->pattern[$offset - 1] === '/' && (!isset($this->pattern[$offset + $length]) || $this->pattern[$offset + $length] === '/')) {
                        $tr["/<$name>"] = "(/(?P<$placeholder>$pattern))?";
                    } else {
                        $tr["<$name>"] = "(?P<$placeholder>$pattern)?";
                    }
                } else {
                    $tr["<$name>"] = "(?P<$placeholder>$pattern)";
                }
                if (isset($this->_routeParams[$name])) {
                    $tr2["<$name>"] = "(?P<$placeholder>$pattern)";
                } else {
                    $this->_paramRules[$name] = $pattern === '[^\/]+' ? '' : "#^$pattern$#u";
                }
            }
        }
        $this->_template = \preg_replace('/<([\w._-]+):?([^>]+)?>/', '<$1>', $this->pattern);
        $this->pattern = '#^' . \trim(\strtr($this->_template, $tr), '/') . '$#u';
        if (!empty($this->_routeParams)) {
            $this->_routeRule = '#^' . \strtr($this->route, $tr2) . '$#u';
        }
    }
    /**
     * Parses the given request and returns the corresponding route and parameters.
     * @param UrlManager $manager the URL manager
     * @param Request $request the request component
     * @return array|boolean the parsing result. The route and the parameters are returned as an array.
     * If false, it means this rule cannot be used to parse this path info.
     */
    public function parseRequest($manager, $request)
    {
        if ($this->mode === self::CREATION_ONLY) {
            return false;
        }
        if (!empty($this->verb) && !\in_array($request->getMethod(), $this->verb, true)) {
            return false;
        }
        $pathInfo = $request->getPathInfo();
        $pathInfo = \rtrim($pathInfo,'/');
        $suffix = (string)($this->suffix === null ? $manager->suffix : $this->suffix);
        if ($suffix !== '' && $pathInfo !== '') {
            $n = \strlen($suffix);
            if (\substr_compare($pathInfo, $suffix, -$n, $n) === 0) {
                $pathInfo = \substr($pathInfo, 0, -$n);
                if ($pathInfo === '') {
                    // suffix alone is not allowed
                    return false;
                }
            } else {
                return false;
            }
        }
        if ($this->host !== null) {
            $pathInfo = \strtolower($request->getHostInfo()) . ($pathInfo === '' ? '' : '/' . $pathInfo);
        }
        if (!\preg_match($this->pattern, $pathInfo, $matches)) {
            return false;
        }
        $matches = $this->substitutePlaceholderNames($matches);
        foreach ($this->defaults as $name => $value) {
            if (!isset($matches[$name]) || $matches[$name] === '') {
                $matches[$name] = $value;
            }
        }
        $params = $this->defaults;
        $tr = [];
        foreach ($matches as $name => $value) {
            if (isset($this->_routeParams[$name])) {
                $tr[$this->_routeParams[$name]] = $value;
                unset($params[$name]);
            } elseif (isset($this->_paramRules[$name])) {
                $params[$name] = $value;
            }
        }
        if ($this->_routeRule !== null) {
            $route = \strtr($this->route, $tr);
        } else {
            $route = $this->route;
        }

        \Yii::trace("Request parsed with URL rule: {$this->name}", __METHOD__);
        return [$route, $params];
    }
}