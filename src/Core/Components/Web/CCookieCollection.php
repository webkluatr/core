<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 10:59
 */

namespace Kluatr\Core\Components\Web;

use yii\web\CookieCollection as BaseCookie;
class CCookieCollection extends BaseCookie
{
    private $_cookies =[];
    /**
     * Adds a cookie to the collection.
     * If there is already a cookie with the same name in the collection, it will be removed first.
     * @param Cookie $cookie the cookie to be added
     * @throws InvalidCallException if the cookie collection is read only
     */
    public function add($cookie)
    {
        if ($this->readOnly) {
            throw new InvalidCallException('The cookie collection is read only.');
        }
        $this->_cookies[$cookie->name] = $cookie;
        \Yii::$app->response->setCookies();
    }
}