<?php
/**
 * Created by PhpStorm.
 * User: p.bunaev
 * Date: 12.12.2017
 * Time: 11:02
 */

namespace Kluatr\Core\Components\Web;
use yii\base\Component;
class AApplication  extends Component{
    public function init(){
        #Yii::$app->errorHandler->errorAction='stat/main/error';

        if (!empty(\Yii::$app->modules)) {
            $modules = \Yii::$app->modules;
            // Смотрим зарегистрированные модули системы
            foreach($modules as $moduleName=>$data){
                $module = \Yii::$app->getModule($moduleName);
                if (!empty($module->urlRules)) {
                    $urlRules = $module->urlRules;
                }
                //Добавляем роутинг из модуля
                if (!empty($urlRules))
                    \Yii::$app->getUrlManager()->addRules($urlRules);
            }
            return true;
        }

    }
}
