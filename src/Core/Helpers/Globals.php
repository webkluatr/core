<?php
namespace Kluatr\Core\Helpers;

function parseDirectory($folder,$prefix,$namespaces){
    $arr = [];
    foreach(\glob($folder."*",\GLOB_ONLYDIR) as $dir){
        //$pref = ($namespasing === true) ? $prefix.\ucfirst(\basename($dir)) :"";
        $pref = $prefix.\ucfirst(\basename($dir));
        parse_file_directory($arr,$dir,$pref,false,$namespaces);
    }
    return $arr;
}
function parsePhpFiles($folder,$prefix,$namespaces){
    $arr = [];
    parse_file_directory($arr,$folder,$prefix,true,$namespaces);
    return $arr;
}
function parse_file_directory(&$arr,$dir,$pref,$phpOnly,$namespaces){
    foreach ((\glob($dir. \DS ."*".(($phpOnly)?".php":""))) as $file){
        if(\is_dir($file)===true){
            $data = ($phpOnly===false)
                ? parseDirectory($file,$namespaces===true?$pref."\\":$pref."_",$namespaces)
                : parsePhpFiles($file,$namespaces===true?$pref."\\":$pref."_",$namespaces);
            $arr = \array_merge($arr,$data);
            continue;
        }
        $class = ($namespaces === true)? $pref."\\".\basename($file,".php") : $pref."_".\basename($file,".php");
        $arr[$class] = $file;
    }
}

/**
 * распаковывает строку в объект
 * test comment
 * @param string $value
 * @return mixed
 */
function un_packaging_string_to_object($value){
    $unbase = \base64_decode($value,true);
    if($unbase === false) return null;
    $unflate = @\gzinflate ($unbase);
    if($unflate === false) return null;
    $unserelize = \json_decode ($unflate);
    if($unserelize === false) return null;
    return $unserelize;
}
/**
 * запаковывает объект в строку
 * @param mixed $value
 * @return string
 */
function packaging_object_to_string($value){
    return \base64_encode(\gzdeflate(\json_encode($value)));;
}

/**
 * Cast an object into a different class.
 *
 * Currently this only supports casting DOWN the inheritance chain,
 * that is, an object may only be cast into a class if that class
 * is a descendant of the object's current class.
 *
 * This is mostly to avoid potentially losing data by casting across
 * incompatable classes.
 *
 * @param object $object The object to cast.
 * @param string $class The class to cast the object into.
 * @return object
 */
function cast($object, $class) {
    if( !\is_object($object) )
        throw new \InvalidArgumentException('$object must be an object.');
    if( !\is_string($class) )
        throw new \InvalidArgumentException('$class must be a string.');
    if( !\class_exists($class) )
        throw new \InvalidArgumentException(\sprintf('Unknown class: %s.', $class));
    if( !\is_subclass_of($class, \get_class($object)) )
        throw new \InvalidArgumentException(\sprintf(
            '%s is not a descendant of $object class: %s.',
            $class, \get_class($object)
        ));
    /**
     * This is a beautifully ugly hack.
     *
     * First, we serialize our object, which turns it into a string, allowing
     * us to muck about with it using standard string manipulation methods.
     *
     * Then, we use preg_replace to change it's defined type to the class
     * we're casting it to, and then serialize the string back into an
     * object.
     */
    return \unserialize(
        \preg_replace(
            '/^O:\d+:"[^"]++"/',
            'O:'.\strlen($class).':"'.$class.'"',
            \serialize($object)
        )
    );
}

function shutdown() {
    $error = \error_get_last();
    \sleep(0);
    //if ($error['type'] === \E_ERROR) {
    var_dump($error);
    echo "FAIL!!";
    \var_dump(\debug_backtrace());
    //}
}