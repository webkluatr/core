<?php
/**
 * Created by PhpStorm.
 * User: pbunaev
 * Date: 23.01.18
 * Time: 16:00
 */

namespace Tests\Core\Components\Errors;
use PHPUnit\Framework\TestCase;

use Kluatr\Core\Components\Errors\BaseException;

final class BaseExceptionTest extends TestCase
{
    public function testGetErrorStringInfo()
    {
        $error1 = new BaseException("error first");
        $error2 = new BaseException("error last",500,$error1);
        $info1 = $error2->getErrorStringInfo();
        $info2 = "";
        while(true){
            $info2 .= \str_replace(["-code-","-message-","-file-","-line-","-stack-"],
                [$error2->getCode(),$error2->getMessage(),$error2->getFile(),$error2->getLine(),$error2->getTraceAsString()],$error2->getErrorTpl());
            $error2 = $error2->getPrevious();
            if (empty($error2) === true) break;
            $info2 .= "\n INNER ERROR:";
        }
        $this->assertEquals($info1,$info2,"не полный вывод информации по исключению");
    }
}